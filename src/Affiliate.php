<?php

declare(strict_types=1);

namespace Megabonus\Laravel\Affiliate;

use Exception;
use Megabonus\Laravel\Affiliate\Clients\TaoBaoClient;
use Megabonus\Laravel\Affiliate\Contracts\Check;
use Megabonus\Laravel\Affiliate\Services\Check\CheckService;
use Megabonus\Laravel\Affiliate\Services\Client\ClientParserService;
use Megabonus\Laravel\Affiliate\Services\Country\CountryService;

class Affiliate implements Check
{
    private $checkService;

    private $client;

    private $parserService;

    private $countryService;

    public function __construct(CheckService $checkService,
                                TaoBaoClient $taoBaoClient,
                                ClientParserService $clientParserService,
                                CountryService $countryService
    ){
        $this->checkService = $checkService;
        $this->countryService = $countryService;
        $this->client = $taoBaoClient;
        $this->parserService = $clientParserService;
    }

    /**
     * {@inheritdoc}
     * @throws Exception
     */
    public function check(string $link, bool $needSave, string $country = null): bool
    {
        $this->checkService->checkConfig();

        $this->checkService->checkLink($link);

        $link = $this->checkService->getReferenceLink($link);

        $model = $this->checkService->getModel($link);

        if($model instanceof \stdClass){
            if(strtotime($model->{config('affiliate.has_affiliate_links_table.columns.updated_at')}) >
                strtotime("-". config('affiliate.expire_in_days'). " day")){
                return true;
            }
        }

        $id = $this->checkService->getItemId($link);

        $response = $this->client->request($id);

        $affiliate = $this->parserService->checkParse(json_decode(json_encode($response), true));

        $dataItem = $this->parserService->getProductData();

        $config = $this->countryService->getConfigByRule($link, $country);

        $dataItem['affiliate'] = $this->countryService->checkAffiliateByConfig($config, $affiliate, $dataItem['is_exception'] ?? false);

        if($needSave){
            $this->checkService->insertSaveRow($dataItem, $link);
        }

        return $dataItem['affiliate'];
    }

    /**
     * @param array $links
     * @param bool $needSave
     * @return bool
     */
    public function checkArray(array $links, bool $needSave): bool
    {
        return false;
    }
}