<?php

declare(strict_types=1);

namespace Megabonus\Laravel\Affiliate\Exceptions;

use InvalidArgumentException;
use Throwable;

class ConfigException extends InvalidArgumentException
{
    /**
     * @param  string  $message
     * @param  int  $code
     * @param  Throwable|null  $previous
     */
    public function __construct(
        $message = 'Config error',
        $code = 0,
        Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return ConfigException
     */
    public static function apiKey(): ConfigException
    {
        return new static('undefined api key');
    }

    /**
     * @return ConfigException
     */
    public static function clientSecret(): ConfigException
    {
        return new static('undefined client secret');
    }

    /**
     * @return ConfigException
     */
    public static function trackingId(): ConfigException
    {
        return new static('undefined tracking Id');
    }

    /**
     * @return ConfigException
     */
    public static function HasAffiliateLinksTable(): ConfigException
    {
        return new static('undefined tracking has affiliate links table');
    }

    /**
     * @return ConfigException
     */
    public static function HasAffiliateLinksTableColumns(): ConfigException
    {
        return new static('undefined tracking has affiliate links table columns');
    }

    /**
     * @return ConfigException
     */
    public static function HasShopExceptionsTable(): ConfigException
    {
        return new static('undefined tracking has shop exceptions table');
    }

    /**
     * @return ConfigException
     */
    public static function HasShopExceptionsTableColumns(): ConfigException
    {
        return new static('undefined tracking has shop exceptions table columns');
    }

    /**
     * @return ConfigException
     */
    public static function SNG(): ConfigException
    {
        return new static('undefined rule_for_countries config');
    }

    /**
     * @return ConfigException
     */
    public static function noSNG(): ConfigException
    {
        return new static('undefined rule_for_other_countries config');
    }
}
