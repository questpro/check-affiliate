<?php

namespace Megabonus\Laravel\Affiliate\Services\Country;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Megabonus\Laravel\Affiliate\Exceptions\ParserException;

class CountryService
{
    /**
     * @param string $url
     * @param string|null $country
     * @return array
     */
    public function getConfigByRule(string $url, ?string $country): array
    {
        if($country && in_array(strtoupper($country), config('affiliate.countries_for_rule'))){
            return config('affiliate.rule_for_countries');
        }

        $host = parse_url($url, PHP_URL_HOST);
        $hostPieces = explode('.', $host);
        $topLevelDomain = strtolower(end($hostPieces));

        if($topLevelDomain == 'ru'){
            return config('affiliate.rule_for_countries');
        }

        return config('affiliate.rule_for_other_countries');
    }

    /**
     * @param array $config
     * @param bool $isAffiliate
     * @param bool $isException
     * @return bool
     */
    public function checkAffiliateByConfig(array $config, bool $isAffiliate, bool $isException): bool
    {
        if ($config['isNeedCheckAffiliate'] && $config['isNeedCheckExceptions']) {
            return ($isAffiliate > 0 && !$isException);
        } elseif (!$config['isNeedCheckAffiliate'] && $config['isNeedCheckExceptions']) {
            return !$isException;
        } elseif ($config['isNeedCheckAffiliate'] && !$config['isNeedCheckExceptions']) {
            return $isAffiliate;
        } else return true;
    }
}