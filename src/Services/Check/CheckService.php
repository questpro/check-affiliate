<?php

namespace Megabonus\Laravel\Affiliate\Services\Check;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Megabonus\Laravel\Affiliate\Exceptions\ConfigException;
use Megabonus\Laravel\Affiliate\Exceptions\LinkException;
use Megabonus\Laravel\Affiliate\Exceptions\ShopException;

class CheckService
{
    /**
     * @param string $link
     * @return void
     *
     * @throws LinkException
     */
    public function checkLink(string $link): void
    {
        $parseLink = parse_url($link);

        if(!isset($parseLink['host']) || !isset($parseLink['scheme']) || !isset($parseLink['path'])){
            throw LinkException::url();
        }

        if(!in_array($parseLink['host'], config('affiliate.access_hosts'))){
            throw LinkException::host();
        }
    }

    /**
     * @param string $link
     * @return \stdClass|null
     */
    public function getModel(string $link): ? \stdClass
    {
        return DB::table(config('affiliate.has_affiliate_links_table.table'))
            ->where(config('affiliate.has_affiliate_links_table.columns.url'), $this->buildLink($link))
            ->where(config('affiliate.has_affiliate_links_table.columns.affiliate'), 1)
            ->first();
    }

    /**
     * @param array $data
     * @param string $url
     * @return void
     */
    public function insertSaveRow(array $data, string $url): void
    {
        DB::table(config('affiliate.has_affiliate_links_table.table'))->updateOrInsert(
            [
                config('affiliate.has_affiliate_links_table.columns.url') => $url,
            ],
            [
                config('affiliate.has_affiliate_links_table.columns.url') => $url,
                config('affiliate.has_affiliate_links_table.columns.affiliate') => $data['affiliate'] ?? 0,
                config('affiliate.has_affiliate_links_table.columns.commission_rate') => $data['commission_rate'] ?? 0,
                config('affiliate.has_affiliate_links_table.columns.relevant_market_commission_rate') => $data['relevant_market_commission_rate'] ?? 0,
                config('affiliate.has_affiliate_links_table.columns.shop_id') => $data['shop_id'] ?? 0,
                config('affiliate.has_affiliate_links_table.columns.created_at') => date('Y-m-d H:i:s'),
                config('affiliate.has_affiliate_links_table.columns.updated_at') => date('Y-m-d H:i:s'),
            ]
        );
    }

    public function checkConfig(): void
    {
        if(strlen(config('affiliate.has_affiliate_links_table.table')) == 0){
            throw ConfigException::HasAffiliateLinksTableColumns();
        }

        if(strlen(config('affiliate.has_affiliate_links_table.columns.url')) == 0){
            throw ConfigException::HasAffiliateLinksTableColumns();
        }

        if(strlen(config('affiliate.has_affiliate_links_table.columns.affiliate')) == 0){
            throw ConfigException::HasAffiliateLinksTableColumns();
        }

        if(strlen(config('affiliate.has_affiliate_links_table.columns.commission_rate')) == 0){
            throw ConfigException::HasAffiliateLinksTableColumns();
        }

        if(strlen(config('affiliate.has_affiliate_links_table.columns.relevant_market_commission_rate')) == 0){
            throw ConfigException::HasAffiliateLinksTableColumns();
        }

        if(strlen(config('affiliate.has_affiliate_links_table.columns.shop_id')) == 0){
            throw ConfigException::HasAffiliateLinksTableColumns();
        }

        if(strlen(config('affiliate.has_affiliate_links_table.columns.created_at')) == 0){
            throw ConfigException::HasAffiliateLinksTableColumns();
        }

        if(strlen(config('affiliate.has_affiliate_links_table.columns.updated_at')) == 0){
            throw ConfigException::HasAffiliateLinksTableColumns();
        }

        if(strlen(config('affiliate.has_shop_exceptions_table.column_name')) == 0){
            throw ConfigException::HasShopExceptionsTable();
        }

        if(strlen(config('affiliate.has_shop_exceptions_table.table')) == 0){
            throw ConfigException::HasShopExceptionsTableColumns();
        }


        if(strlen(config('affiliate.rule_for_countries.isNeedCheckAffiliate')) == 0 || strlen(config('affiliate.rule_for_countries.isNeedCheckExceptions')) == 0){
            throw ConfigException::SNG();
        }

        if(strlen(config('affiliate.rule_for_other_countries.isNeedCheckAffiliate')) == 0 || strlen(config('affiliate.rule_for_other_countries.isNeedCheckExceptions')) == 0){
            throw ConfigException::noSNG();
        }
    }


    /**
     * @param string $link
     * @return string
     */
    public function getItemId(string $link): string
    {
        preg_match_all('/\d+/', $link, $matches);

        return end($matches[0]);
    }

    /**
     * @param string $link
     * @return string
     * @throws Exception
     */
    public function getReferenceLink(string $link): string
    {
        $link = explode('?', $link)[0];

        $host = parse_url($link, PHP_URL_HOST);

        return $this->getRedirectUrl($link, $host);
    }

    /**
     * @param string $url
     * @param string $host
     * @return string
     * @throws Exception
     */
    function getRedirectUrl(string $url, string $host): string
    {
        $check_short_url = get_headers($url, 1);

        if(isset($check_short_url['Location'])){
            $referenceUrl = is_array($check_short_url['Location'])
                ? $this->getItemLocationArray($host, $check_short_url['Location'])
                : $check_short_url['Location'];

            $parseUrl = parse_url($referenceUrl);

            return $parseUrl['scheme'] . '://' . $parseUrl['host'] . $parseUrl['path'];
        }

        return $url;
    }

    /**
     * @param string $host
     * @param array $locations
     * @return string
     * @throws Exception
     */
    private function getItemLocationArray(string $host, array $locations): string
    {
        foreach ($locations as $location){
            if(str_contains($location, '//' . $host . '/')){
                return $location;
            }
        }

        throw new Exception('Undefined host locations');
    }

    /**
     * @param string $link
     * @return string
     */
    private function buildLink(string $link): string
    {
        $parseLink = parse_url($link);

        return $parseLink['scheme'].'://'.$parseLink['host'].$parseLink['path'];
    }
}