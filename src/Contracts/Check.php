<?php

namespace Megabonus\Laravel\Affiliate\Contracts;

interface Check
{
    /**
     * @param string $link
     * @param bool $needSave
     * @param string|null $country
     * @return bool
     */
    public function check(string $link, bool $needSave, string $country = null): bool;


    /**
     * @param array $links
     * @param bool $needSave
     * @return mixed
     */
    public function checkArray(array $links, bool $needSave);
}
